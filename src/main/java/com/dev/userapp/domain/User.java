/**
 * 
 */
package com.dev.userapp.domain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author devendra.singh
 *
 */
@Document(collection = "user")
public class User {

	/**
	 * 
	 */
	@Id
	private String id;
	private String username;
    private String password;
    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	private List topicList;
	

	public List getTopicList() {
		return topicList;
	}

	public void setTopicList(List topicList) {
		this.topicList = topicList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	

	public User(String id, String username, List topicList) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.username=username;
		this.topicList=topicList;

	}

	

}
