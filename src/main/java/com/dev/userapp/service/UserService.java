/**
 * 
 */
package com.dev.userapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.userapp.domain.User;
import com.dev.userapp.repository.UserRepositery;

/**
 * @author devendra.singh
 *
 */
@Service
public class UserService {

	/**
	 * 
	 */
	@Autowired
	UserRepositery userRepository;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	public Optional<User> getUserById(String id) {
		// TODO Auto-generated method stub
		//return topicList.stream().filter(topic -> topic.getId().equals(id)).findFirst().get();
		return userRepository.findById(id);
	}
	
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		//return topicList.stream().filter(topic -> topic.getId().equals(id)).findFirst().get();
		return userRepository.findAll();
	}
	

}
