package com.dev.userapp.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dev.userapp.domain.User;
import com.dev.userapp.repository.UserRepositery;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	UserRepositery userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//Optional<com.dev.userapp.domain.User> user =   userRepository.findById(username);
		
        Optional<com.dev.userapp.domain.User> user =  userRepository.findById(username);
		
		User user1 = user.get();
	
		  if (user1.getId().equals(username)) {
			  
			  return new org.springframework.security.core.userdetails.User (user1.getId(),user1.getPassword(), new ArrayList<>()); 

		   } 
		  else { 
			  throw new UsernameNotFoundException("User not found with username: " + username); 
		  
		  }
		
	}
}

