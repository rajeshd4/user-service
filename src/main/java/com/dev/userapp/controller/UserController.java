/**
 * 
 */
package com.dev.userapp.controller;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


import com.dev.userapp.domain.User;
import com.dev.userapp.service.UserService;

/**
 * @author devendra.singh
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/api")
public class UserController {

	/**
	 * 
	 */
	@Autowired
	UserService userService; 
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * @GetMapping("/user/{userid}") public Optional<User>
	 * getUserById(@PathVariable("userid") String userid){ return
	 * userService.getUserById(userid);
	 * 
	 * //return userService.ge }
	 */
	
	@GetMapping("/user/allUsers")
	public List<User> getAllUsers() throws Exception{
		System.out.println("I am in All user#############");
		return userService.getAllUser();
	}
	
	
	@RequestMapping("/user/{id}")
	public Optional<User> getUserById(@PathVariable("id") String id){

		return userService.getUserById(id);
	}
	
	@GetMapping("/user/allUserTopic")
	public String getTopic() throws RestClientException, IOException {
		
		String url = "http://localhost:8181/api/topics";
		
		RestTemplate  restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;
		
		try {
			response = restTemplate.exchange(url,HttpMethod.GET,getHeader(), String.class);
		    
			System.out.println(response.getBody());
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
		
		
		return response.getBody().toString();
		
		
	}

	private static HttpEntity<?> getHeader() throws IOException{
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept",MediaType.APPLICATION_JSON_VALUE );
		return new HttpEntity<>(headers);
	}
	
	//Get Topic by User id
	
	@GetMapping("/user/topicByUserId/{userId}")
	public String getTopicByUserId(@PathVariable("userId") String userId) throws Exception {
		
		//String userId = "test123";
		//String topicsUrl = "http://java-microservice-app1-1289745246.us-west-2.elb.amazonaws.com:8181/api/topics/user/";
		ResponseEntity<String> response = null;
		
		List<User> userList =  getAllUsers();
		
		List<String> userHolder = new ArrayList<>();
		for (User listObj : userList) {
			userHolder.add(listObj.getId());
		    
		}
		userHolder.forEach(System.out::println);
		
		if(userHolder.contains(userId)) {
		
		String url = "http://java-microservice-app1-1289745246.us-west-2.elb.amazonaws.com:8181/api/topics/user/"+userId.trim();	
        //String url = "http://localhost:8181/api/topics/user/"+userId.trim();
		
		RestTemplate  restTemplate = new RestTemplate();
		
		
		try {
			response = restTemplate.exchange(url,HttpMethod.GET,getHeader(), String.class);
		    
			System.out.println(response.getBody());
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
		}
		return response.getBody().toString();
		
		}
}
